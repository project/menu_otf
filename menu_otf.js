   Authored by Matt Westgate <drupal at asitis dot org>
*/

function stopPropagation(e) {
  if (!e) e=window.event;
  if (typeof e.preventDefault!='undefined') e.preventDefault();
  if (typeof e.stopPropagation!='undefined') e.stopPropagation();
  return false;
}

function addEvent(o,e,f) {
  if (o.addEventListener) { o.addEventListener(e,f,false); return true; }
  else if (o.attachEvent) { return o.attachEvent('on'+e,f); }
  return false;
}

String.prototype.trim = function() {
  return this.replace('^\s+','').replace('\s+$','');
}

var REQUIRED = 'required';
var ERROR = 'error';
var COLLAPSED = 'fieldset-collapsed';
var EXPANDED = 'fieldset-expanded';
var IS_COLLAPSED = 'collapsed';

addEvent(window, 'load', initFieldsets);

function initFieldsets() {
  if (document.getElementsByTagName) {
    var f = document.getElementsByTagName('fieldset');
    for (var i=0; i<f.length; i++) {
      if (f[i].id == 'quick-menu') {
        var l = f[i].getElementsByTagName('legend')[0];
        addEvent(l, 'click', fieldsetClickHandler);
        addEvent(l, 'mousedown', stopPropagation);
        addEvent(l, 'mousemove', stopPropagation);
        f[i].isCollapsible = fieldsetIsCollapsible;
        var p = new RegExp('[^\w-]*'+IS_COLLAPSED+'[^\w-]*');
        if (f[i].className.match(p) && f[i].isCollapsible()) {
          f[i].className = f[i].className.replace(p, ' ' + COLLAPSED);
        }
        else {
          f[i].className += ' ' + EXPANDED;
        }
      }
    }
  }
}

function fieldsetClickHandler(e) {
  if (!e) e = window.event;
  if (this.parentNode) {
    var f = this.parentNode;
  }
  else if (e.srcElement && e.srcElement.parentNode) {
    var f = e.srcElement.parentNode;
  }
  var pe = new RegExp('[^\w-]*'+EXPANDED+'[^\w-]*');
  var pc = new RegExp('[^\w-]*'+COLLAPSED+'[^\w-]*');
  if (f.className.match(pc)) {
    f.className = f.className.replace(pc, ' '+EXPANDED+' ');
  }
  else if (f.className.match(pe) && f.isCollapsible()) {
    f.className = f.className.replace(pe, ' '+COLLAPSED+' ');
  }
  return false;
}

function fieldsetIsCollapsible() {
  var n = this.getElementsByTagName('*');
  for (var i=0; i < n.length; i++) {
    if (n[i].className && (n[i].className.match('[^\w-]*'+ERROR+'[^\w-]*') ||
              (n[i].className.match('[^\w-]*'+REQUIRED+'[^\w-]*')))) {
      if (n[i].tagName.toLowerCase() == 'input' && n[i].value.trim() == '') {
        return false;
      }
      else if (n[i].tagName.toLowerCase() == 'select' && n[i].options[n[i].selectedIndex].value.trim() == '') {
        return false;
      }
    }
  }
  return true;
}


function setTitle(form) {
  // Prefill the menu item title to the current node title.
  if (form['edit[menu][title]'].value == '') {
    form['edit[menu][title]'].value = form['edit[title]'].value;
  }
}
