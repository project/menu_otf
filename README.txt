********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: menu_otf module
Author: Matt Westgate <drupal at asitis dot org>
Last update: (See CHANGELOG.txt for details)
Drupal: 4.5
Dependencies:
  menu.module (comes with Drupal base installation)
********************************************************************
DESCRIPTION:

Provides an easy way to add nodes to the menu system.

With the introduction of Drupal 4.5 came the menu module which lets 
an user create menus of links for site navigation or other purposes. 
While this is a great addition to the Drupal system, the workflow 
could use some refining.  Here is the current workflow:

1. Create content.
2. Remember the node id of the newly created content (node).
3. Navigate to 'administer > menu > add menu item' and build the link.

After enabling menu_otf, the workflow becomes.

1. While creating content (a node), optionally choose a title for 
   the link for it to show up in one of the selected site menus.

This module lets you create menu items while creating content. It
also lets you manage menu links while editing existing content and 
eliminates the need to go to a separate interface to manage menus.

********************************************************************
DEPENDENCIES:

You must enable menu.module. Menu_otf uses the save routines and 
permissions from menu.module and will not work without it.

********************************************************************
INSTALLATION:

see the INSTALL.txt file in this directory.

********************************************************************
WISH LIST:

- The ability to suppress some of the drupal_set_messages() outputted 
  by menu.module. 

********************************************************************
HOW TO USE THIS MODULE

Read this after installing the module.

Using this module involves three steps.

  1. Navigate to a node form for story, page, blog, or some other 
  	 node type.
  2. Type in a title and body.
  3. If you'd like put this entry into the menu system, click 
  	 the "Site navigation settings" link to display the menu options.

Once the content is created with a menu item, you can edit and 
even delete menu items in the same fashion.
